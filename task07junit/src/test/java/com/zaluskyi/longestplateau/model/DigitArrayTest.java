package com.zaluskyi.longestplateau.model;

import org.apache.logging.log4j.core.util.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DigitArrayTest {
    private DigitArray digitArray = new DigitArray();

    @Test
    void getStringDigit() {
        String actual = "string digit";
        digitArray.setStringDigit(actual);
        String expected = digitArray.getStringDigit();
        assertEquals(expected, actual);
    }

    @Test
    void setStringDigit() {
        String actual = "sting digit";

    }

    @Test
    void getDigitArray() {
    }

    @Test
    void setDigitArray() {
    }

    @Test
    void setDigitArrayFromString() {
    }

    @Test
    void findLengthContiguousSequenceDigitStartingWithIndex() {
    }

    @Test
    void findStartIndexTheMostDigitPlateau() {
    }
}
