package com.zaluskyi.minesweeper.view;

import com.zaluskyi.minesweeper.controller.Controller;

import java.util.Scanner;

public class View {
    private Controller controller = new Controller();
    private Scanner input = new Scanner(System.in);


    public void start() {
        String choice;
        while (true) {
            menu();
            choice = input.next();
            switch (choice) {
                case "1":
                    controller.getGameBoard();
                    break;
                case "2":
                    controller.printGameBoard();
                    break;
                default:
                    return;

            }
        }
    }

    private void menu() {
        System.out.println("        M E N U");
        System.out.println("1 - enter game board\n2 - print game board\nE X I T - press any key");
    }
}
