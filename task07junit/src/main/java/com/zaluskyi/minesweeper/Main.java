package com.zaluskyi.minesweeper;

import com.zaluskyi.minesweeper.model.GameBoard;
import com.zaluskyi.minesweeper.view.View;

public class Main {
    public static GameBoard gameBoard = new GameBoard();

    public static void main(String[] args) {
        new View().start();
    }
}
