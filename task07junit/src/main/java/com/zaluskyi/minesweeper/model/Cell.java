package com.zaluskyi.minesweeper.model;

public class Cell {
    private boolean status;
    private String sign;
    private int numberNeighboringBombs;

    public Cell() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public int getNumberNeighboringBombs() {
        return numberNeighboringBombs;
    }

    public void setNumberNeighboringBombs(int numberNeighboringBombs) {
        this.numberNeighboringBombs = numberNeighboringBombs;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "status=" + status +
                ", numberNeighboringBombs=" + numberNeighboringBombs +
                '}';
    }
}
