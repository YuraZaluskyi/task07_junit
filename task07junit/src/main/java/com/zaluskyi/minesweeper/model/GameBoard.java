package com.zaluskyi.minesweeper.model;

import java.util.Arrays;
import java.util.Random;

public class GameBoard {
    private int m = 3;
    private int n = 5;
    private int p = 7;
    private Cell[][] cells = new Cell[m][n];

    public GameBoard() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                cells[i][j] = new Cell();
            }
        }
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    //methods////////////////////////////////////////////////////////////////////////////////////////////////////////

    private int getRandomNumber() {
        Random random = new Random();
        return random.nextInt(p + 15);
    }

    public void fillCellsBombs() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                int randomNumber = getRandomNumber();
                if (randomNumber < p) {
                    cells[i][j].setStatus(true);
                    cells[i][j].setSign(" * ");
                } else {
                    cells[i][j].setStatus(false);
                    cells[i][j].setSign(" # ");
                }
            }
        }
    }

    public void printGameBoard() {
        int row = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("   " + i);
        }
        System.out.println("");
        for (int i = 0; i < cells.length; i++) {
            System.out.print(row + " ");
            for (int j = 0; j < cells[i].length; j++) {
                System.out.print(cells[i][j].getSign() + " ");
            }
            row++;
            System.out.println("");
        }
    }


    @Override
    public String toString() {
        return "GameBoard{" +
                "m=" + m +
                ", n=" + n +
                ", p=" + p +
                ", cells=" + Arrays.toString(cells) +
                '}';
    }
}
