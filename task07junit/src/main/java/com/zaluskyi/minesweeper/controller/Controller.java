package com.zaluskyi.minesweeper.controller;

import com.zaluskyi.minesweeper.model.GameBoard;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Controller {
    private GameBoard gameBoard = new GameBoard();
    private Scanner input = new Scanner(System.in);

    public void getGameBoard() {
        int quantityRows;
        int quantityColumns;
        try {
            System.out.println("enter quantity rows");
            quantityRows = input.nextInt();
            System.out.println("enter quantity columns");
            quantityColumns = input.nextInt();
            gameBoard.setM(quantityRows);
            gameBoard.setN(quantityColumns);
        } catch (InputMismatchException e) {
            System.out.println("enter number!");
        }
        gameBoard.fillCellsBombs();
    }

    public void printGameBoard() {
        gameBoard.printGameBoard();
    }

}
