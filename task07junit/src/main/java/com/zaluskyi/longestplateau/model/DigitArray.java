package com.zaluskyi.longestplateau.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class DigitArray {
    private static Logger logger = LogManager.getLogger(DigitArray.class);
    private String stringDigit = new String();
    private int[] digitArray = new int[0];

    public DigitArray() {
    }

    public DigitArray(String stringDigit, int[] digitArray) {
        this.stringDigit = stringDigit;
        this.digitArray = digitArray;
    }

    public DigitArray(int[] digitArray) {
        this.digitArray = digitArray;
    }

    public String getStringDigit() {
        return stringDigit;
    }

    public void setStringDigit(String stringDigit) {
        this.stringDigit = stringDigit;
    }

    public int[] getDigitArray() {
        return digitArray;
    }

    public void setDigitArray(int[] digitArray) {
        this.digitArray = digitArray;
    }

    //methods///////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setDigitArrayFromString() {
        if (isStringDigit()) {
            digitArray = new int[stringDigit.length()];
            getDigitArrayFromString();
        } else {
            logger.warn("enter only digit");
        }
    }

    private boolean isStringDigit() {
        char[] chars = stringDigit.toCharArray();
        for (char c : chars) {
            if (c < 48 || c > 57) {
                return false;
            }
        }
        return true;
    }

    private void getDigitArrayFromString() {
        char[] chars = stringDigit.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            digitArray[i] = Integer.parseInt(String.valueOf(chars[i]));
        }
    }

    private boolean isPresentContiguousSequenceDigit(int index) {
        for (int i = index; i < digitArray.length - 1; i++) {
            if (digitArray[i] == digitArray[i + 1]) {
                return true;
            }
        }
        return false;
    }

    public int findLengthContiguousSequenceDigitStartingWithIndex(int index) {
        int length = 1;
        for (int i = index; i < digitArray.length - 1; i++) {
            if (digitArray[i] == digitArray[i + 1]) {
                while ((i < digitArray.length - 1) && (digitArray[i] == digitArray[i + 1])) {
                    length++;
                    i++;
                }
                return length;
            }
        }
        return length;
    }

    private int findStartIndexContiguousSequenceDigitStartingWithIndex(int index) {
        for (int i = index; i < digitArray.length - 1; i++) {
            if (digitArray[i] == digitArray[i + 1]) {
                return i;
            }
        }
        return -1;
    }

    private boolean isContiguousSequenceDigitPlateau(int index, int length) {
        if (length == digitArray.length) {
            return false;
        } else if ((index == 0) && digitArray[length] < digitArray[length - 1]) {
            return true;
        } else if ((index == digitArray.length - length) && digitArray[index - 1] < digitArray[index]) {
            return true;
        } else if ((index != 0) && (index != digitArray.length - length)) {
            if ((digitArray[index - 1] < digitArray[index])
                    && (digitArray[index + length] < digitArray[index + length - 1])) {
                return true;
            }
        }
        return false;
    }

    public int findStartIndexTheMostDigitPlateau() {
        if (digitArray.length < 3) {
            logger.warn("Enter more 2 elements");
            return 0;
        }
        int maxLength = 1;
        int indexMaxPlateau = -1;
        int index = findStartIndexContiguousSequenceDigitStartingWithIndex(0);
        if (index == -1) {
            logger.warn("Contiguous Sequence Digit is is missing");
            return 0;
        }
        while ((isPresentContiguousSequenceDigit(index)) || index < digitArray.length - 1) {
            int startIndexContiguousSequence = findStartIndexContiguousSequenceDigitStartingWithIndex(index);
            int lengthContiguousSequence = findLengthContiguousSequenceDigitStartingWithIndex
                    (startIndexContiguousSequence);
            boolean isDigitPlateau = isContiguousSequenceDigitPlateau
                    (startIndexContiguousSequence, lengthContiguousSequence);
            if (isDigitPlateau) {
                if (lengthContiguousSequence > maxLength) {
                    maxLength = lengthContiguousSequence;
                    indexMaxPlateau = startIndexContiguousSequence;
                    index = startIndexContiguousSequence + lengthContiguousSequence;
                } else {
                    index = startIndexContiguousSequence + lengthContiguousSequence;
                }
            } else {
                index = startIndexContiguousSequence + lengthContiguousSequence;
            }
        }
        return indexMaxPlateau;
    }

    @Override
    public String toString() {
        return "DigitArray{" +
                "stringDigit='" + stringDigit + '\'' +
                ", digitArray=" + Arrays.toString(digitArray) +
                '}';
    }
}
