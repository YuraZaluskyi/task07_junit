package com.zaluskyi.longestplateau;

import com.zaluskyi.longestplateau.view.View;

public class Main {
    public static void main(String[] args) {
        new View().start();

    }
}
