package com.zaluskyi.longestplateau.controller;

import com.zaluskyi.longestplateau.model.DigitArray;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);
    private Scanner input = new Scanner(System.in);
    private DigitArray digitArray = new DigitArray();

    public Controller() {
    }

    public void enterSequenceDigit() {
        logger.info("Enter sequence digit");
        digitArray.setStringDigit(input.next());
        digitArray.setDigitArrayFromString();
    }

    public void findTheLongestPlateau() {
        int indexPlateau = digitArray.findStartIndexTheMostDigitPlateau();
        int lengthPlateau = digitArray.findLengthContiguousSequenceDigitStartingWithIndex(indexPlateau);
        logger.info("index plateau {}", indexPlateau);
        logger.info("length plateau {}", lengthPlateau);
    }
}
