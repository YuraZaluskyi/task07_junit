package com.zaluskyi.longestplateau.view;

import com.zaluskyi.longestplateau.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private Scanner input = new Scanner(System.in);
    private Controller controller = new Controller();

    public void start() {
        String choice;
        while (true) {
            menu();
            choice = input.next();
            switch (choice) {
                case "1":
                    controller.enterSequenceDigit();
                    break;
                case "2":
                    controller.findTheLongestPlateau();
                    break;
                default:
                    return;
            }
        }
    }

    public void menu() {
        logger.info("       M E N U");
        logger.info("1 - enter sequence digit");
        logger.info("2 - find length the longest plateau and staring position");
        logger.info("E X I T - press any key");
    }
}
